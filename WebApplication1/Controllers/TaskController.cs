﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;
using System.Text;

namespace WebApplication1.Controllers
{
    public class TaskController : ApiController
    {
        // GET: api/Task
        public IEnumerable<TaskModel> Get()
        {

            string cs = ConfigurationManager.ConnectionStrings["webapiconnection"].ConnectionString;
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
            comm.CommandText = "select Task_ID,Parent_Task,Task,[Start_Date],End_Date,[Priority] from Task inner join ParentTask on ParentTask.Parent_ID=Task.Parent_ID";
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            List<TaskModel> taskList = new List<TaskModel>();
            while (dr.Read())
            {
                TaskModel task = new TaskModel();

                task.Task_ID = int.Parse(dr["Task_ID"].ToString());
                task.Task = dr["Task"].ToString().Trim();
                task.Parent_Task = dr["Parent_Task"].ToString().Trim();
                task.Priority = int.Parse(dr["Priority"].ToString());
                task.Start_Date = DateTime.Parse(dr["Start_Date"].ToString());
                task.End_Date = DateTime.Parse(dr["End_Date"].ToString());
                taskList.Add(task);
            }
            conn.Close();

            return taskList;
        }

        public TaskModel Get(int id)
        {
            string cs = ConfigurationManager.ConnectionStrings["webapiconnection"].ConnectionString;
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
            comm.CommandText = "select Task_ID,ParentTask.Parent_ID,Parent_Task,Task,[Start_Date],End_Date,[Priority] from Task inner join ParentTask on ParentTask.Parent_ID=Task.Parent_ID where Task_ID=" + id;
            conn.Open();
            SqlDataReader dr = comm.ExecuteReader();
            TaskModel task = new TaskModel();
            while (dr.Read())
            {
                task.Task_ID = int.Parse(dr["Task_ID"].ToString());
                task.Task = dr["Task"].ToString().Trim();
                task.Parent_Task = dr["Parent_Task"].ToString().Trim();
                task.Priority = int.Parse(dr["Priority"].ToString());
                task.Start_Date = DateTime.Parse(dr["Start_Date"].ToString());
                task.End_Date = DateTime.Parse(dr["End_Date"].ToString());
                task.Parent_ID= int.Parse(dr["Parent_ID"].ToString());
            }
            conn.Close();

            return task;
        }

        // POST: api/Task
        public string Post([FromBody]TaskModel value)
        {
            string task = value.Task;
            string parentTask = value.Parent_Task;
            int priority = value.Priority;
            string startdate = value.Start_Date.ToString("MM/dd/yyyy");
            string enddate = value.End_Date.ToString("MM/dd/yyyy");

            StringBuilder sb=new StringBuilder();
            sb.Append("declare  @PID int= ISNULL((select max(Parent_ID) from ParentTask), 0)");
            sb.Append("declare  @TID int= ISNULL((select max(Task_ID) from Task), 0)");
            sb.Append("insert into ParentTask values (@PID+1,'"+ parentTask + "')");
            sb.Append("insert into Task values(@TID+1,@PID+1,'"+ task + "','"+ startdate + "','"+ enddate + "',"+ priority + ")");
            string cs = ConfigurationManager.ConnectionStrings["webapiconnection"].ConnectionString;
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
            comm.CommandText = sb.ToString();
            conn.Open();
            int countInserted = comm.ExecuteNonQuery();
            conn.Close();
            return "Data Saved Successfully";
        }

        // PUT: api/Task/5
        public string Put(int id, [FromBody]TaskModel value)
        {
            string task = value.Task;
            int parentId = value.Parent_ID;
            string parentTask = value.Parent_Task;
            int priority = value.Priority;
            string startdate = value.Start_Date.ToString("MM/dd/yyyy");
            string enddate = value.End_Date.ToString("MM/dd/yyyy");
            string cs = ConfigurationManager.ConnectionStrings["webapiconnection"].ConnectionString;
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand comm = new SqlCommand();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("update Task set Task = '"+ task + "',[Start_Date] = '"+ startdate + "', End_Date = '"+ enddate + "',[Priority] = "+ priority + " where task_id = "+ id + "");
            sb.AppendLine("declare @parentTask varchar(500)=(select Parent_Task from ParentTask where Parent_ID="+ parentId + ")");
            sb.AppendLine("update ParentTask set Parent_Task='"+ parentTask + "' where Parent_Task=@parentTask");
            comm.Connection = conn;
            comm.CommandText = sb.ToString();
            conn.Open();
            int countInserted = comm.ExecuteNonQuery();
            conn.Close();
            return "Data Updated Successfully";
        }
        [HttpPut]
        [Route("api/UpdateEndTask/{id}")]
        public string UpdateEndTask(int id, [FromBody]TaskModel value)
        {
            string enddate = DateTime.Now.ToString("MM/dd/yyyy");
            string cs = ConfigurationManager.ConnectionStrings["webapiconnection"].ConnectionString;
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand comm = new SqlCommand();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("update Task set End_Date = '" + enddate + "' where task_id = " + id + "");
            comm.Connection = conn;
            comm.CommandText = sb.ToString();
            conn.Open();
            int countInserted = comm.ExecuteNonQuery();
            conn.Close();
            return "Task Ended Successfully";
        }

        // DELETE: api/Task/5
        public void Delete(int id)
        {
        }
    }
}
