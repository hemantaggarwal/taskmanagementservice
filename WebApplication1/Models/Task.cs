﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class TaskModel
    {
        public int Task_ID { get; set; }
        public string Task { get; set; }
        public int Parent_ID { get; set; }
        public string Parent_Task { get; set; }
        public int Priority { get; set; }
        public DateTime Start_Date { get; set; }
        public DateTime End_Date { get; set; }
    }
}