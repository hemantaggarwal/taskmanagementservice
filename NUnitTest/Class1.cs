﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WebApplication1.Controllers;
using WebApplication1.Models;

namespace NUnitTest
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void GetTasks()
        {
            //Arrange
            TaskController taskController = new TaskController();
            IEnumerable<TaskModel> taskListactualResult = new List<TaskModel>();

            //ACT
            taskListactualResult = taskController.Get();

            //Assert
            int taskListexpectedResult = 1;
            Assert.That(taskListactualResult.Count(), Is.GreaterThanOrEqualTo(taskListexpectedResult));

        }
        [Test]
        public void GetSpecificTask()
        {
            //Arrange
            TaskController taskController = new TaskController();
            TaskModel taskListactualResult = new TaskModel();

            //ACT
            taskListactualResult = taskController.Get(1);

            //Assert
            int taskIdexpectedResult = 1;
            Assert.That(taskListactualResult.Task_ID, Is.EqualTo(taskIdexpectedResult));
        }

        [Test]
        public void PostSpecificTask()
        {
            //Arrange
            TaskController taskController = new TaskController();
            TaskModel task = new TaskModel();
            task.Task_ID = 100;
            task.Parent_ID = 100;
            task.Task = "Unit testing task";
            task.Parent_Task = "Unit testing Parent Task";
            task.End_Date = DateTime.Parse("31-01-2019");
            task.Start_Date= DateTime.Parse("31-03-2019");
            task.Priority = 25;

            //ACT
            string actualResult= taskController.Post(task);

            //Assert
            string expectedResult = "Data Saved Successfully";
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }

        [Test]
        public void PutSpecificTask()
        {
            //Arrange
            TaskController taskController = new TaskController();
            TaskModel task = new TaskModel();
            task.Task_ID = 100;
            task.Parent_ID = 100;
            task.Task = "Unit testing task Update";
            task.Parent_Task = "Unit testing Parent Task Update";
            task.End_Date = DateTime.Parse("10-02-2019");
            task.Start_Date = DateTime.Parse("31-05-2019");
            task.Priority = 20;

            //ACT
            string actualResult = taskController.Put(task.Task_ID,task);

            //Assert
            string expectedResult = "Data Updated Successfully";
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }

        [Test]
        public void UpdateEndTask()
        {
            //Arrange
            TaskController taskController = new TaskController();
            TaskModel task = new TaskModel();
            task.Task_ID = 100;
            task.Parent_ID = 100;
            task.Task = "Unit testing task Update";
            task.Parent_Task = "Unit testing Parent Task Update";
            task.End_Date = DateTime.Parse("31-01-2019");
            task.Start_Date = DateTime.Parse("31-01-2019");
            task.Priority = 20;

            //ACT
            string actualResult = taskController.UpdateEndTask(task.Task_ID, task);

            //Assert
            string expectedResult = "Task Ended Successfully";
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }

    }
}
